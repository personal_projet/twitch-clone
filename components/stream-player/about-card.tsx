"use client";

import {VerifiedMark} from "@/components/verified-mark";
import {BioModal} from "@/components/stream-player/bio-modal";

interface AboutCardProps {
  hostName: string;
  hostIdentity: string;
  viewerIdentity: string;
  bio: string | null;
  followerByCount: number;
}

export const AboutCard = ({hostName, hostIdentity, viewerIdentity, followerByCount, bio}: AboutCardProps) => {
  const hostViewer = `host-${hostIdentity}`;
  const isHost = viewerIdentity === hostViewer;

  const followedByLabel = followerByCount === 1 ? "follower" : "followers";

  return (
    <div className="px-4">
      <div className="group rounded-xl bg-background p-6 lg:p-10 flex flex-col gap-x-3">
        <div className="flex items-center justify-between">
          <div className="flex items-center gap-x-2 font-semibold text-lg">
            About {hostName}
            <VerifiedMark/>
          </div>
          {isHost && (
            <BioModal
              initialValue={bio}
            />
          )}
        </div>
        <div className="text-sm text-muted-foreground">
          <span className="font-semibold text-primary">{followerByCount}</span> {followedByLabel}
        </div>
        <p className="text-sm">
          {bio || "This user has no bio."}
        </p>
      </div>
    </div>
  )
}