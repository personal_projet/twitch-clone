"use client";

import {MinimizeIcon} from "lucide-react";
import {Hint} from "@/components/hint";

interface FullscreenControlProps {
  isFullscreen: boolean;
  toggleFullscreen: () => void;
}

export const FullscreenControl = ({isFullscreen, toggleFullscreen}: FullscreenControlProps) => {
  const Icon = isFullscreen ? MinimizeIcon : MinimizeIcon;

  const label = isFullscreen ? "Exit fullscreen" : "Enter fullscreen";

  return (
    <div className="flex items-center gap-4">
      <Hint label={label} asChild>
        <button
          onClick={toggleFullscreen}
          className="text-white p-1.5 hover:bg-white/10 rounded-lg"
        >
          <Icon
            className="w-5 h-5"
            onClick={toggleFullscreen}
          />
        </button>
      </Hint>

    </div>
  )
}