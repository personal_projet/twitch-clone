"use client";

import {Dialog, DialogClose, DialogContent, DialogHeader, DialogTitle, DialogTrigger} from "@/components/ui/dialog";
import {Button} from "@/components/ui/button";
import {Textarea} from "@/components/ui/textarea";
import {ElementRef, useRef, useState, useTransition} from "react";
import {updateUser} from "@/actions/user";
import {toast} from "sonner";

interface BioModalProps {
  initialValue: string | null;
}

export const BioModal = ({initialValue}: BioModalProps) => {
  const closeRef = useRef<ElementRef<"button">>(null);

  const [isPending, startTransition] = useTransition();
  const [value, setValue] = useState(initialValue);

  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    startTransition(() => {
      updateUser({bio: value})
        .then(() => {
          toast.success("Bio updated successfully");
          closeRef?.current?.click();
        })
        .catch(() => toast.error("Failed to update bio"));
    });
  };
  return (
    <Dialog>
      <DialogTrigger asChild>
        <Button variant="link" size="sm" className="ml-auto">
          Edit
        </Button>
      </DialogTrigger>
      <DialogContent>
        <DialogHeader>
          <DialogTitle>Edit user bio</DialogTitle>
          <form onSubmit={onSubmit} className="space-y-4">
            <Textarea
              placeholder="Tell us about yourself"
              value={value || ""}
              onChange={e => setValue(e.target.value)}
              disabled={isPending}
              className="resize-none"
            />
            <div className="flex justify-between">
              <DialogClose ref={closeRef} asChild>
                <Button type="button" variant="ghost">
                  Cancel
                </Button>
              </DialogClose>
              <Button variant="primary" type="submit" disabled={isPending}>
                Save
              </Button>
            </div>
          </form>
        </DialogHeader>
      </DialogContent>

    </Dialog>
  )
}