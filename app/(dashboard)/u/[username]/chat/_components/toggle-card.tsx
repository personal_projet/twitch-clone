"use client";

import { Switch } from "@/components/ui/switch";
import {useTransition} from "react";
import {updateStream} from "@/actions/stream";
import {toast} from "sonner";
import {Skeleton} from "@/components/ui/skeleton";

type FieldTypes = "isChatEnabled" | "isChatDelayed" | "isChatFollowersOnly";

interface ToggleCardProps {
  field: FieldTypes;
  label: string;
  value: boolean;
}
export const ToggleCard = ({field, value, label} : ToggleCardProps) => {
  const [isPending, startTransition] = useTransition()

  const onChange = () => {
    startTransition(() => {
      updateStream({[field]: !value})
        .then(() => toast.success("Stream updated"))
        .catch(() => toast.error("Failed to update stream"));
    });
  };

  return(
    <div className="rounded-xl p-6 bg-muted">
      <div className="flex items-center justify-between">
        <p className="font-semibold shrink-0">{label}</p>
        <div className="space-y-2">
          <Switch
            disabled={isPending}
            onClick={onChange}
            checked={value}
          >
            {value ? "On" : "Off"}
          </Switch>
        </div>
      </div>
    </div>
  )
}

export const ToggleCardSkeleton = () => {
  return (
    <Skeleton className="rounded-xl p-10 w-full"/>
  );
};