"use client";

import {useTransition} from "react";
import {onUnblock} from "@/actions/block";
import {toast} from "sonner";
import {Button} from "@/components/ui/button";

interface UnblockButtonProps {
  userId: string;
}

export const UnblockButton = ({userId}: UnblockButtonProps) => {
  const [isPending, startTransition] = useTransition();

  const onClick = () => {
    startTransition(() => {
      onUnblock(userId)
        .then(() => toast.success(`Unblocked ${userId}`))
        .catch(() => toast.error(`Failed to unblock ${userId}`));
    });
  };

  return (
    <Button
      variant="link"
      size="sm"
      className="text-violet-600"
      onClick={onClick}
      disabled={isPending}
    >
      Unblock
    </Button>
  )
}