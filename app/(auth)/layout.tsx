import React from 'react';
import {Logo} from "@/app/(auth)/_components/logo";
interface LayoutProps {
  children: React.ReactNode
}

const Layout = ({children} : LayoutProps) => {
  return(
    <div className="h-full flex flex-col justify-center items-center">
      <Logo/>
      {children}
    </div>
  )
}

export default Layout;