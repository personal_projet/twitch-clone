import type {Metadata} from 'next'
import {Inter} from 'next/font/google'
import './globals.css'
import {ClerkProvider} from '@clerk/nextjs'
import {dark} from '@clerk/themes'
import {ThemeProvider} from "@/components/theme-provider";
import {Toaster} from "sonner";

const inter = Inter({subsets: ['latin']})

export const metadata: Metadata = {
  title: 'NextStream - The Next Generation of Streaming',
  description: 'NextStream is the next generation of streaming. It\'s a platform for creators to share their content with the world.',
  icons: [{
    url: "/icon.svg",
    href: "/icon.svg",
  }]


}

export default function RootLayout({
                                     children,
                                   }: {
  children: React.ReactNode
}) {
  return (
    <ClerkProvider appearance={{baseTheme: dark}}>
      <html lang="en">
      <body className={inter.className}>
      <ThemeProvider
        attribute={'class'}
        forcedTheme="dark"
        storageKey='nextstream-theme'
      >
        <Toaster theme="light" position={"bottom-center"}/>
        {children}
      </ThemeProvider>
      </body>
      </html>
    </ClerkProvider>
  )
}
