"use client";

import {Button} from "@/components/ui/button";
import {onFollow, onUnfollow} from "@/actions/follow";
import {useTransition} from "react";
import {toast} from "sonner";
import {onBlock, onUnblock} from "@/actions/block";

interface ActionsProps {
  isFollowing: boolean;
  userId: string;
}

export const Actions = ({isFollowing, userId}: ActionsProps) => {
  const [isPending, startTransition] = useTransition()

  const handleFollow = () => {
    startTransition(() => {
      onFollow(userId)
        .then((data) => toast.success(`You are now following ${data.following.username}`))
        .catch(() => toast.error("Failed to follow the user"))
    })
  }

  const handleUnFollow = () => {
    startTransition(() => {
      onUnfollow(userId)
        .then((data) => toast.success(`You are unfollowed ${data.following.username}`))
        .catch(() => toast.error("Failed to follow the user"))
    })
  }

  const onClick = isFollowing ? handleUnFollow : handleFollow

  const handleBlock = () => {
    startTransition(() => {
      onBlock(userId)
        .then((data) => toast.success(`You are now blocking ${data?.blocked.username}`))
        .catch(() => toast.error("Failed to block the user"))
    });
  };

  return (
    <>
      <Button
        disabled={isPending}
        variant="primary"
        onClick={onClick}
      >
        {isFollowing ? "Unfollow" : "Follow"}
      </Button>
      <Button
        onClick={handleBlock}
        disabled={isPending}
      >
        Block
      </Button>
    </>
  )
}